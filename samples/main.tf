terraform {
  backend "s3" {
    bucket         = "masf-brlink-dev-terraform-states-bucket" # it needs to be change
    key            = "infraestrutura/aws-backup.tfstate"
    region         = "eu-west-1"                                  # it needs to be change correct one it is ca-central-1
    dynamodb_table = "masf-brlink-dev-terraform-state-lock-table" # it needs to be change
    #profile        = "nome_do_projeto_dev"  # it needs to be change
  }
}

locals {
  region = "eu-west-1"
  vpc_id = "vpc-021f15add0fb928f7"
}

provider "aws" {
  region = local.region
}

data "aws_vpc" "selected" {
  id = local.vpc_id
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.selected.id
}

module "aurora_rds_sample" {
  source = "../module"

  project_name        = "brlink"
  cluster_identifier  = "test-aurora"
  environment         = "dev"
  database_name       = "test"
  is_test_mode        = true # Only to test not use in production
  deletion_protection = false

  max_capacity = 2

  vpc_id = local.vpc_id

  subnet_ids = ["subnet-07805bb2c4457f4df", "subnet-08062d9ed8a78bbcb"] # data.aws_subnet_ids.all.ids

}
