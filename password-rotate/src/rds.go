package main

import (
	"log"
	"os"
	"strconv"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/rds"
)

type RDShManager struct {
	_session *session.Session
}

func NewRDShManager(currentSession *session.Session) *RDShManager {
	return &RDShManager{currentSession}
}

func (resource *RDShManager) GetPasswordGenerateOptions() *PasswordGenerateOptions {
	passwordLength, err := strconv.ParseInt(os.Getenv("PASSWORD_LENGTH"), 10, 64)
	if err != nil {
		passwordLength = 32
	}

	return &PasswordGenerateOptions{
		ExcludeLowercase:        false,
		ExcludeNumbers:          false,
		ExcludePunctuation:      true,
		ExcludeUppercase:        false,
		IncludeSpace:            false,
		PasswordLength:          passwordLength,
		RequireEachIncludedType: true,
	}
}

func (resource *RDShManager) UpdatePassword(clusterId string, newPassword *SecretValue) {
	svc := rds.New(resource._session)

	input := &rds.ModifyDBClusterInput{}

	input.SetDBClusterIdentifier(clusterId)
	input.SetMasterUserPassword(newPassword.Password)
	input.SetApplyImmediately(true)

	if _, err := svc.ModifyDBCluster(input); err != nil {
		log.Printf("Cannot update the Password in cluster: %s\n", clusterId)
		log.Fatalf("AWS error: %s\n", err.Error())
	}
}
