package main

import (
	"log"
	"os"
	"strconv"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/elasticache"
)

type ElasticacheManager struct {
	_session *session.Session
}

func NewElasticacheManager(currentSession *session.Session) *ElasticacheManager {
	return &ElasticacheManager{currentSession}
}

func (resource *ElasticacheManager) GetPasswordGenerateOptions() *PasswordGenerateOptions {
	passwordLength, err := strconv.ParseInt(os.Getenv("PASSWORD_LENGTH"), 10, 64)
	if err != nil {
		passwordLength = 32
	}

	return &PasswordGenerateOptions{
		ExcludeLowercase:        false,
		ExcludeNumbers:          false,
		ExcludePunctuation:      true,
		ExcludeUppercase:        false,
		IncludeSpace:            false,
		PasswordLength:          passwordLength,
		RequireEachIncludedType: true,
	}
}

func (resource *ElasticacheManager) UpdatePassword(clusterId string, newPassword *SecretValue) {
	svc := elasticache.New(resource._session)

	input := &elasticache.ModifyReplicationGroupInput{
		ApplyImmediately:        aws.Bool(true),
		ReplicationGroupId:      aws.String(clusterId),
		AuthToken:               aws.String(newPassword.Password),
		AuthTokenUpdateStrategy: aws.String("ROTATE"),
	}

	if _, err := svc.ModifyReplicationGroup(input); err != nil {
		log.Printf("Cannot update the AuthToken in cluster: %s\n", clusterId)
		log.Fatalf("AWS error: %s\n", err.Error())
	}

}
