package main

type Step string

func (step Step) String() string {
	return string(step)
}

const (
	// CreateSecret specifies the secret creation step.
	CreateSecret Step = "createSecret"
	// SetSecret specifies the secret setting step.
	SetSecret Step = "setSecret"
	// TestSecret specifies the secret testing step.
	TestSecret Step = "testSecret"
	// FinishSecret specifies the secret finishing step.
	FinishSecret Step = "finishSecret"
)

type LambdaInput struct {
	Step               Step   `json:"Step"`
	SecretID           string `json:"SecretId"`
	ClientRequestToken string `json:"ClientRequestToken"`
}

type VersionStage string

func (versionStage VersionStage) String() string {
	return string(versionStage)
}

const (
	// AWSCURRENT is a version stage tag.
	AWSCURRENT VersionStage = "AWSCURRENT"
	// AWSPENDING is a version stage tag.
	AWSPENDING VersionStage = "AWSPENDING"
	// AWSPREVIOUS is a version stage tag.
	AWSPREVIOUS VersionStage = "AWSPREVIOUS"
)

type SecretValue struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type IResourceManager interface {
	GetPasswordGenerateOptions() *PasswordGenerateOptions
	UpdatePassword(clusterId string, newPassword *SecretValue)
}

type PasswordGenerateOptions struct {
	ExcludeLowercase        bool
	ExcludeNumbers          bool
	ExcludePunctuation      bool
	ExcludeUppercase        bool
	IncludeSpace            bool
	PasswordLength          int64
	RequireEachIncludedType bool
}
