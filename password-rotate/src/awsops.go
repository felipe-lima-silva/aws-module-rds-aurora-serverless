package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
)

// OpsClient is used to interact with AWS.
type OpsClient struct {
	region              string
	lambdaInput         *LambdaInput
	_session            *session.Session
	_secretsmanager     *secretsmanager.SecretsManager
	_versionIDsToStages map[string][]*string
	_resourceManager    IResourceManager
}

// NewOpsClient returns a new AWS Ops client.
func NewOpsClient(region string, lambdaInput *LambdaInput) *OpsClient {
	return &OpsClient{
		region:      region,
		lambdaInput: lambdaInput,
	}
}

func (client *OpsClient) getSession() *session.Session {
	if client._session == nil {
		client._session = session.Must(session.NewSession(&aws.Config{
			MaxRetries:                    aws.Int(3),
			CredentialsChainVerboseErrors: aws.Bool(true),
			Region:                        aws.String(client.region),
		}))
	}
	return client._session
}

func (client *OpsClient) getSecretsManager() *secretsmanager.SecretsManager {
	if client._secretsmanager == nil {
		client._secretsmanager = secretsmanager.New(client.getSession())
	}
	return client._secretsmanager
}

// CreateSecret tests secret.
func (client *OpsClient) CreateSecret() {
	var (
		stepName              = "CREATE_SECRET"
		currentSecretValue    *SecretValue
		currentValueVersionID string
		pendingSecretValue    *SecretValue
		pendingValueVersionID string
		err                   error
	)
	log.Printf("(%s) Start creating secret for secret ID %s...\n", stepName, client.lambdaInput.SecretID)

	if pendingSecretValue, pendingValueVersionID, err = client.getSecretValue(AWSPENDING); err == nil {
		log.Printf("(%s) Successfully got pending secret with version ID: %s\n", stepName, pendingValueVersionID)
	} else if currentSecretValue, currentValueVersionID, err = client.getSecretValue(AWSCURRENT); err != nil {
		log.Fatalf("(%s) Failed to get current secret; %s\n", stepName, err.Error())
	} else {

		log.Printf("(%s) Successfully got current secret with version ID: %s\n", stepName, currentValueVersionID)
		*pendingSecretValue = *currentSecretValue
		pendingSecretValue.Password = client.getRandomPassword(client.getResourceManager().GetPasswordGenerateOptions())

		pendingValueVersionID = client.putSecretValue(pendingSecretValue, AWSPENDING)
		log.Printf("(%s) Successfully put pending secret with version ID: %s\n", stepName, pendingValueVersionID)
	}
}

// SetSecret tests secret.
func (client *OpsClient) SetSecret() {
	var (
		stepName              = "SET_SECRET"
		pendingSecretValue    *SecretValue
		pendingValueVersionID string
		err                   error
	)
	log.Printf("(%s) Start setting secret for secret ID %s...\n", stepName, client.lambdaInput.SecretID)

	if _, _, err = client.getSecretValue(AWSCURRENT); err != nil {
		log.Fatalf("(%s) Failed to get current secret; %s\n", stepName, err.Error())
	} else if pendingSecretValue, pendingValueVersionID, err = client.getSecretValue(AWSPENDING); err != nil {
		log.Fatalf("(%s) Failed to get pending secret; %s\n", stepName, err.Error())
	}

	client.getResourceManager().UpdatePassword(os.Getenv("CLUSTER_ID"), pendingSecretValue)

	log.Printf("(%s) Successfully updated build executor OS user secret with version ID: %s\n", stepName, pendingValueVersionID)
}

// TestSecret tests secret.
func (client *OpsClient) TestSecret() {

}

// FinishSecret finishes secret.
func (client *OpsClient) FinishSecret() {
	var (
		stepName         = "FINISH_SECRET"
		currentVersionID = client.getVersionIDFromVersionStage(AWSCURRENT)
		pendingVersionID = client.getVersionIDFromVersionStage(AWSPENDING)
		err              error
	)
	log.Printf("(%s) Start finishing secret for secret ID %s...\n", stepName, client.lambdaInput.SecretID)

	if currentVersionID == client.lambdaInput.ClientRequestToken {
		log.Printf("(%s) Secret version with client request token already set as AWSCURRENT for version ID: %s\n", stepName, currentVersionID)
	} else if err = client.moveVersionStage(AWSCURRENT, currentVersionID, pendingVersionID); err != nil {
		log.Fatalf("(%s) Failed to update version stage; %s\n", stepName, err.Error())
	} else {
		log.Printf("(%s) Successfully updated AWSCURRENT from %#v to %#v.\n", stepName, currentVersionID, pendingVersionID)
	}
}

func (client *OpsClient) getResourceManager() IResourceManager {
	if client._resourceManager != nil {
		return client._resourceManager
	}

	resourceType := os.Getenv("ENGINE")

	if resourceType == "elasticache" {
		client._resourceManager = NewElasticacheManager(client._session)
	} else if resourceType == "opensearch" {
		client._resourceManager = NewOpenSearchManager(client._session)
	} else if resourceType == "rds" {
		client._resourceManager = NewRDShManager(client._session)
	} else {
		log.Fatalf("Resource type '%s' not mapped", resourceType)
	}

	return client._resourceManager
}

func (client *OpsClient) getSecretValue(versionStage VersionStage) (secretValue *SecretValue, versionID string, err error) {
	var (
		verStgStr = versionStage.String()
		verID     = client.getVersionIDFromVersionStage(versionStage)
		input     = &secretsmanager.GetSecretValueInput{
			SecretId:     &client.lambdaInput.SecretID,
			VersionId:    &verID,
			VersionStage: &verStgStr,
		}
		output *secretsmanager.GetSecretValueOutput
		errMsg string
	)
	secretValue = &SecretValue{}
	if len(verID) == 0 {
		errMsg = fmt.Sprintf("Unable to find secret value with version ID: %s", versionID)
	} else if output, err = client.getSecretsManager().GetSecretValue(input); err != nil {
		errMsg = fmt.Sprintf("Unable to get secret value; %s", err.Error())
	} else if err = json.Unmarshal([]byte(*output.SecretString), secretValue); err != nil {
		errMsg = fmt.Sprintf("Unable to unmarshal secret value; %s", err.Error())
	} else {
		versionID = *output.VersionId
	}
	if err = nil; len(errMsg) > 0 {
		log.Printf("[IGNORABLE] Error upon Getting Secret Value: %s\n", errMsg)
		err = errors.New(errMsg)
	}
	return
}

func (client *OpsClient) putSecretValue(secretValue *SecretValue, versionStage VersionStage) (versionID string) {
	var (
		verStgStr  = versionStage.String()
		bytes, err = json.Marshal(secretValue)
		secret     = string(bytes)
		input      = &secretsmanager.PutSecretValueInput{
			SecretId:           &client.lambdaInput.SecretID,
			SecretString:       &secret,
			VersionStages:      []*string{&verStgStr},
			ClientRequestToken: &client.lambdaInput.ClientRequestToken,
		}
		output *secretsmanager.PutSecretValueOutput
	)
	if err != nil {
		log.Fatalf("Failed to marshal secret value; %s\n", err.Error())
	} else if output, err = client.getSecretsManager().PutSecretValue(input); err != nil {
		log.Fatalf("Failed to put secret value; %s\n", err.Error())
	} else {
		versionID = *output.VersionId
	}
	return
}

func (client *OpsClient) moveVersionStage(versionStage VersionStage, fromVerID, toVerID string) (err error) {
	var (
		verStgStr = versionStage.String()
		input     = &secretsmanager.UpdateSecretVersionStageInput{
			MoveToVersionId:     &toVerID,
			RemoveFromVersionId: &fromVerID,
			SecretId:            &client.lambdaInput.SecretID,
			VersionStage:        &verStgStr,
		}
		errMsg string
	)
	if _, err = client.getSecretsManager().UpdateSecretVersionStage(input); err != nil {
		errMsg = fmt.Sprintf("Failed to move version stage %s from %#v to %#v: %s", versionStage.String(), fromVerID, toVerID, err.Error())
	}
	if err = nil; len(errMsg) > 0 {
		err = errors.New(errMsg)
	}
	return
}

func (client *OpsClient) getRandomPassword(passwordGenerateOptions *PasswordGenerateOptions) string {
	var (
		input = &secretsmanager.GetRandomPasswordInput{
			ExcludeLowercase:        aws.Bool(passwordGenerateOptions.ExcludeLowercase),
			ExcludeNumbers:          aws.Bool(passwordGenerateOptions.ExcludeNumbers),
			ExcludePunctuation:      aws.Bool(passwordGenerateOptions.ExcludePunctuation),
			ExcludeUppercase:        aws.Bool(passwordGenerateOptions.ExcludeUppercase),
			IncludeSpace:            aws.Bool(passwordGenerateOptions.IncludeSpace),
			PasswordLength:          aws.Int64(passwordGenerateOptions.PasswordLength),
			RequireEachIncludedType: aws.Bool(passwordGenerateOptions.RequireEachIncludedType),
		}
		output *secretsmanager.GetRandomPasswordOutput
		err    error
	)
	if output, err = client.getSecretsManager().GetRandomPassword(input); err != nil {
		log.Fatalf("Failed to generate a random password; %s", err.Error())
	}
	return *output.RandomPassword
}

func (client *OpsClient) getVersionIDsToStages() map[string][]*string {
	if len(client._versionIDsToStages) == 0 {
		var (
			input  = &secretsmanager.DescribeSecretInput{SecretId: &client.lambdaInput.SecretID}
			output *secretsmanager.DescribeSecretOutput
			err    error
		)
		if output, err = client.getSecretsManager().DescribeSecret(input); err != nil {
			log.Fatalf("Failed to describe secret with secret ID: %s\n", client.lambdaInput.SecretID)
		}
		client._versionIDsToStages = output.VersionIdsToStages
	}
	return client._versionIDsToStages
}

func (client *OpsClient) getVersionIDFromVersionStage(versionStage VersionStage) (versionID string) {
	for id, stages := range client.getVersionIDsToStages() {
		for _, stage := range stages {
			if *stage == string(versionStage) {
				versionID = id
				return
			}
		}
	}
	return
}
