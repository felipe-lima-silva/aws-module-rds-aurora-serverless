package main

import (
	"context"
	"os"

	"github.com/aws/aws-lambda-go/lambda"
)

var (
	awsRegion = os.Getenv("AWS_DEFAULT_REGION")
)

func lambdaHandler(context context.Context, input LambdaInput) {
	awsOpsClient := NewOpsClient(awsRegion, &input)
	switch input.Step {
	// Step 1: Create Secret
	case CreateSecret:
		awsOpsClient.CreateSecret()
	// Step 2: Set Secret
	case SetSecret:
		awsOpsClient.SetSecret()
	// Step 3: Test Secret
	case TestSecret:
		awsOpsClient.TestSecret()
	// Step 4: Finish Secret
	case FinishSecret:
		awsOpsClient.FinishSecret()
	}
}

// main is the main method.
func main() {
	lambda.Start(lambdaHandler)
}