package main

import (
	"log"
	"os"
	"strconv"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/opensearchservice"
)

type OpenSearchManager struct {
	_session *session.Session
}

func NewOpenSearchManager(currentSession *session.Session) *OpenSearchManager {
	return &OpenSearchManager{currentSession}
}

func (resource *OpenSearchManager) GetPasswordGenerateOptions() *PasswordGenerateOptions {
	passwordLength, err := strconv.ParseInt(os.Getenv("PASSWORD_LENGTH"), 10, 64)
	if err != nil {
		passwordLength = 32
	}

	return &PasswordGenerateOptions{
		ExcludeLowercase:        false,
		ExcludeNumbers:          false,
		ExcludePunctuation:      false,
		ExcludeUppercase:        false,
		IncludeSpace:            false,
		PasswordLength:          passwordLength,
		RequireEachIncludedType: true,
	}
}

func (resource *OpenSearchManager) UpdatePassword(clusterId string, newPassword *SecretValue) {
	svc := opensearchservice.New(resource._session)

	input := &opensearchservice.UpdateDomainConfigInput{}
	input.SetDomainName(clusterId)

	aso := &opensearchservice.AdvancedSecurityOptionsInput_{}
	muo := &opensearchservice.MasterUserOptions{}

	muo.SetMasterUserPassword(newPassword.Password)
	muo.SetMasterUserName(newPassword.Username)
	aso.SetMasterUserOptions(muo)
	input.SetAdvancedSecurityOptions(aso)

	if _, err := svc.UpdateDomainConfig(input); err != nil {
		log.Printf("Cannot update the Password in cluster: %s\n", clusterId)
		log.Fatalf("AWS error: %s\n", err.Error())
	}
}
