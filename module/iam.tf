resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role       = aws_iam_role.rotate_lambda.name
  policy_arn = aws_iam_policy.rotate_policy.arn
}

resource "aws_iam_role" "rotate_lambda" {
  name               = "Role-Lambda-Rotate-${var.cluster_identifier}-${var.environment}"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

data "aws_iam_policy_document" "assume_role" {
  statement {
    sid    = "AssumeRoleAccess"
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }

    actions = [
      "sts:AssumeRole",
    ]
  }
}

resource "aws_iam_policy" "rotate_policy" {
  name   = "Lambda-Policy-${var.cluster_identifier}-${var.environment}"
  policy = data.aws_iam_policy_document.lambda_rotate_policy.json
}


data "aws_iam_policy_document" "lambda_rotate_policy" {
  statement {
    sid    = "LogsAccess"
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]

    resources = [
      "arn:aws:logs:*:*:*",
    ]
  }

  statement {
    sid    = "SecretManagerAccess"
    effect = "Allow"

    actions = [
      "secretsmanager:DescribeSecret",
      "secretsmanager:GetSecretValue",
      "secretsmanager:PutSecretValue",
      "secretsmanager:UpdateSecretVersionStage",
    ]

    resources = [
      aws_secretsmanager_secret.default.arn,
    ]
  }

  statement {
    sid    = "AllowGenerateRandomPassword"
    effect = "Allow"

    actions = [
      "secretsmanager:GetRandomPassword",
    ]

    resources = ["*"]
  }

  statement {
    sid    = "ElasticacheAccess"
    effect = "Allow"

    actions = [
      "rds:*",
    ]

    resources = [
      aws_rds_cluster.aurora_serverless.arn,
    ]
  }
}
