resource "aws_lambda_function" "default" {
  filename      = var.rotate_lambda.zip
  function_name = local.lambda_name
  role          = aws_iam_role.rotate_lambda.arn
  handler       = var.rotate_lambda.handler

  source_code_hash = filebase64sha256(var.rotate_lambda.zip)

  runtime = "go1.x"

  environment {
    variables = {
      CLUSTER_ID = var.cluster_identifier
      ENGINE = local.service_name
      PASSWORD_LENGTH = var.password_length
    }
  }

  lifecycle {
    ignore_changes = [
      filename, source_code_hash
    ]
  }

  tags = var.tags
}

resource "aws_lambda_permission" "allow_secret_manager" {
  statement_id  = "AllowExecutionFromSecretManager"
  action        = "lambda:InvokeFunction"
  function_name = local.lambda_name
  principal     = "secretsmanager.amazonaws.com"  
}