locals {
  service_name = "rds"
  password                = var.password == null ? random_password.password.result : var.password

  lambda_name = "Secret-Rotate-${var.cluster_identifier}-${var.project_name}-${var.environment}"

  backup_plan = {
    daily = "dailybackup",
    weekly = "weeklybackup",
    monthly = "monthlybackup"
  }

  rds_common_tags = {
    "Name" = var.cluster_identifier,
    lookup(tomap(local.backup_plan), var.backup_plan , element(values(local.backup_plan), 0)) = "yes"
  }
}

resource "random_password" "password" {
  length  = var.password_length
  special = false
}

resource "random_id" "server" {
  byte_length = 8
}

