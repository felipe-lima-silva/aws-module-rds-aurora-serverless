variable "environment" {
  type        = string
  description = "Ambiente do projeto."

  validation {
    condition     = contains(["dev", "hml", "prd"], var.environment)
    error_message = "Valores validos: dev, hml, prd."
  }
}

variable "username" {
  type        = string
  description = "Nome do usuário do RDS Aurora Serverless."
  default     = "master"
}

variable "password" {
  type        = string
  description = "Senha do usuário do RDS Aurora Serverless."
  default     = null
}

variable "deletion_protection" {
  type        = bool
  description = "Habilita a proteção contra deleção do RDS Aurora Serverless."
  default     = true
}

variable "apply_immediately" {
  type        = bool
  description = "Habilita a aplicação de mudanças imediatamente no RDS Aurora Serverless. Quando habilitado, pode gerar indisponibilidade do banco ao realizar mudanças."
  default     = true
}

variable "existing_snapshot_identifier" {
  type        = string
  description = "ID de snapshot existente de cluster RDS para o Terraform puxar os dados."
  default     = ""
}

variable "database_name" {
  type        = string
  description = "Nome da base de dados do RDS Aurora Serverless."
}

variable "cluster_identifier" {
  type        = string
  description = "Identificador do cluster RDS Aurora Serverless."
}

variable "project_name" {
  type        = string
  description = "Nome do projeto. Usado para nomear alguns recursos."
}

variable "vpc_id" {
  type        = string
  description = "Vpc id."
}

variable "subnet_ids" {
  type        = list(string)
  description = "Lista de IDs de subnets."

  validation {
    condition     = length(var.subnet_ids) >= 3
    error_message = "Subnet ids require minimum of 3 subnets."
  }
}

variable "tags" {
  type        = map(any)
  description = "Opcional Map de tags para inserir nos recursos."
  default     = {}
}

variable "password_length" {
  type        = number
  description = "Opcional ao menos que `password` esteja preenchido. Especifica o tamanho da senha a ser inserida. Default 32"
  default     = 32
}

variable "password_rotate_after_days" {
  type        = number
  description = "(Required) Specifies the number of days between automatic scheduled rotations of the secret. Default 30 days"
  default     = 30
}

variable "engine_mode" {
  type        = string
  description = "Especifica qual sera a engine que o RDS ira ser executada. `multimaster`, `parallelquery`, `provisioned`, `serverless`. Defaults: `serverless`"
  default     = "serverless"

  validation {
    condition     = contains(["multimaster", "parallelquery", "provisioned", "serverless"], var.engine_mode)
    error_message = "So pode ser utilizada uma das engine: `multimaster`, `parallelquery`, `provisioned`, `serverless`."
  }
}

variable "max_capacity" {
  type        = number
  description = "Especifica a quantidade maxima de computacao serveless, essa opcao so sera inserida se o engine_mode for `serveless`"
  default     = 32
}

variable "min_capacity" {
  type        = number
  description = "Especifica a quantidade minima de computacao serveless, essa opcao so sera inserida se o engine_mode for `serveless`"
  default     = 1
}


variable "log_retention" {
  type        = number
  description = "Especifica a quantidade de dias que o log sera retido. Possiveis valores: 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, 3653. Se colocar 0, entao os logs nao terao expiracao."
  default     = 14

  validation {
    condition     = contains([1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, 3653], var.log_retention)
    error_message = "So pode ser utilizadaos valores: 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, 3653."
  }
}

variable "backup_window" {
  type        = string
  description = "Especifica a janela de backup do RDS. Default 23:00-07:00"
  default     = "23:00-07:00"
}

variable "backup_plan" {
  type        = string
  description = "(Required) Tipo de backup plan que o RDS ira usar, possiveis valores: `daily`, `weekly`, `monthly`"
  default     = "daily"

  validation {
    condition     = contains(["daily", "weekly", "monthly"], var.backup_plan)
    error_message = "So pode ser utilizada um dos valores: `daily`, `weekly`, `monthly`."
  }
}

variable "backup_retention" {
  type        = number
  description = "Especifica a o numero de dias que o backup automatico sera mantido. Default 7"
  default     = 7
}

variable "is_test_mode" {
  type        = bool
  description = "(TEST MODE) use this option to avoid use the open all ingress access ip"
  default     = false
}

variable "rotate_lambda" {

  type = object({
    zip     = string
    handler = string
  })
  description = "Obrigatorio Lambda responsavel por rotacionar a senha. Default zip = lambda_rotate.zip handler = main"
  default = {
    handler = "main"
    zip     = "lambda_rotate.zip"
  }
}
