resource "aws_secretsmanager_secret" "default" {
  name = var.is_test_mode == false ? "Secret-${var.cluster_identifier}-${var.project_name}-${var.environment}" : "Secret-${var.cluster_identifier}-${var.project_name}-${var.environment}-${random_id.server.id}" 

  tags = var.tags
}

resource "aws_secretsmanager_secret_rotation" "default" {  
  secret_id           = aws_secretsmanager_secret.default.id
  rotation_lambda_arn = aws_lambda_function.default.arn

  rotation_rules {
    automatically_after_days = var.password_rotate_after_days
    
  }
  
  tags = var.tags
}

resource "aws_secretsmanager_secret_version" "default" {  
  secret_id     = aws_secretsmanager_secret.default.id
  secret_string = jsonencode({"username"= var.username, "password"= local.password})
}