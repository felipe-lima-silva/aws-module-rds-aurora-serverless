resource "aws_db_subnet_group" "aurora_serverless" {
  name        = "default-db-sb-group-${lower(var.project_name)}-${var.environment}"
  description = "default"
  subnet_ids  = var.subnet_ids
  tags        = var.tags
}

resource "aws_security_group" "aurora_serverless" {
  name                   = "SG-RDS-Aurora-${var.cluster_identifier}"
  description            = "Security group para o RDS ${var.cluster_identifier}"
  vpc_id                 = var.vpc_id
  revoke_rules_on_delete = true

  egress {
    protocol    = "TCP"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = var.tags
}

resource "aws_rds_cluster" "aurora_serverless" {
  cluster_identifier      = var.cluster_identifier
  engine                  = "aurora-mysql"
  engine_mode             = var.engine_mode
  engine_version          = "5.7.mysql_aurora.2.07.1"
  database_name           = var.database_name
  master_username         = var.username
  master_password         = local.password
  skip_final_snapshot     = true
  vpc_security_group_ids  = [aws_security_group.aurora_serverless.id]
  db_subnet_group_name    = aws_db_subnet_group.aurora_serverless.name
  deletion_protection     = var.deletion_protection
  apply_immediately       = var.apply_immediately
  backup_retention_period = var.backup_retention
  preferred_backup_window = var.backup_window


  enable_http_endpoint = var.is_test_mode && var.engine_mode == "serverless"

  dynamic "scaling_configuration" {
    for_each = var.engine_mode == "serverless" ? [1] : []

    content {
      max_capacity             = var.max_capacity < var.min_capacity ? var.min_capacity : var.max_capacity
      min_capacity             = var.max_capacity < var.min_capacity ? var.max_capacity : var.min_capacity
      timeout_action           = "ForceApplyCapacityChange"
    }
  }

  lifecycle {
    ignore_changes = [scaling_configuration]
  }
  depends_on = [
    aws_cloudwatch_log_group.rds
  ]

  tags = merge(local.rds_common_tags, var.tags)
}
