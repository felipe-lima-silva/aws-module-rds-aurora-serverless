resource "aws_cloudwatch_log_group" "rotate" {
  name = "/aws/lambda/${local.lambda_name}"

  retention_in_days = var.log_retention
  tags              = var.tags
}


resource "aws_cloudwatch_log_group" "rds" {
  name = "/aws/rds/cluster/${var.cluster_identifier}/error"

  retention_in_days = var.log_retention
  tags              = var.tags
}
